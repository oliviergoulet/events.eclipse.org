import React, { useState } from 'react';
import Checkbox from '../Checkbox';
import { alphaOrder } from '../EventHelpers';
import PropTypes from 'prop-types';

const EventsCheckboxFilters = ({ checkedBoxes, setCheckedBoxes, filterOptions, filterGroupTitle }) => {
  const determineInitialState = () => {
    return window.innerWidth > 991;
  };

  const [showTypes, setShowTypes] = useState(determineInitialState());

  const handleChange = (e) => {
    if (e.target.checked) {
      setCheckedBoxes({
        ...checkedBoxes,
        [e.target.name]: e.target.checked,
      });
    } else {
      setCheckedBoxes({
        ...checkedBoxes,
        [e.target.name]: undefined,
      });
    }
  };

  const toggleTypes = () => {
    setShowTypes(!showTypes);
  };

  return (
    <div className="margin-bottom-10">
      <button onClick={toggleTypes} className="event-filter-title">
        {filterGroupTitle}
        <i className="fa fa-angle-down event-filter-expandable-icon" aria-hidden="true"></i>
      </button>
      {showTypes && (
        <ul className="event-filter-checkbox-list">
          {alphaOrder(filterOptions).map((item) => (
            <li key={item.id}>
              <label key={item.id}>
                <Checkbox name={item.id} checked={checkedBoxes[item.id]} onChange={handleChange} />
                {item.name}
              </label>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

EventsCheckboxFilters.propTypes = {
  checkedBoxes: PropTypes.object,
  setCheckedBoxes: PropTypes.func,
};

export default EventsCheckboxFilters;
